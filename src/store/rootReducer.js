import { combineReducers } from 'redux';

// Modules
import userReducer from './modules/user/userReducer';

// Combines all reducers to a single reducer function
const rootReducer = combineReducers({
  userReducer,
});

export default rootReducer;
