import * as types from './userTypes';

export const fetchAllUserAction = () => ({
  type: types.FETCH_ALL_USER_REQUEST,
});

export default fetchAllUserAction;
