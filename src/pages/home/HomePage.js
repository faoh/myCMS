import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  root: {
    display: 'flex',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroudColor: '#fff',
    height: '100vh',
    fontFamily: 'Roboto',
    overflow: 'hidden',
  },
  title: {
    fontSize: 42,
  },
  version: {
    color: '#aaa',
  },
};

class HomePage extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <span className={classes.title}>myCMS&nbsp;</span>
        <span className={classes.version}>v1.0.0</span>
      </div>
    );
  }
}

export default withStyles(styles)(HomePage);
