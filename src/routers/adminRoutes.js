import AdminDefaultLayout from '../layouts/admin/AdminDefaultLayout';
import Dashboard from '../pages/admin/Dashboard';
import UserList from '../pages/admin/User/UserList';
import UserNew from '../pages/admin/User/UserNew';
import SignIn from '../pages/admin/Auth/SignIn';
import SignUp from '../pages/admin/Auth/SignUp';

const adminRoutes = [
  {
    path: '/signin',
    component: SignIn,
  },
  {
    path: '/signup',
    component: SignUp,
  },
  {
    path: '/admin',
    component: AdminDefaultLayout,
    routes: [
      {
        path: '/admin/dashboard',
        component: Dashboard,
      },
      {
        path: '/admin/users',
        component: UserList,
        exact: true,
      },
      {
        path: '/admin/users/new',
        component: UserNew,
        exact: true,
      },
    ],
  },
];

export default adminRoutes;
