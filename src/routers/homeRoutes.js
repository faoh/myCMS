import HomePage from '../pages/home/HomePage';

const homeRoutes = [
  {
    path: '/',
    component: HomePage,
    exact: true,
  },
];

export default homeRoutes;
